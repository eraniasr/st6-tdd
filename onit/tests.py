from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from django.http import HttpRequest
from datetime import date
from .models import Status
import unittest


# Create your tests here.

class Story6UnitTest(TestCase):

    def test_hello_name_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)

    def test_lab_3_using_how_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'Hai.html')

    def test_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_input_status_success(self):
            response = Client().post('/', {"status": "Is sick."}, follow=True)
            
            self.assertEqual(response.status_code,200)
            self.assertEqual(Status.objects.all().count(), 1)
            
            new_response = self.client.get('/')
            htmlresponse = new_response.content.decode('utf8')
            self.assertIn("Is sick.", htmlresponse)

    def test_input_status_failed(self):
            charas = "wat"*300
            response = Client().post('/', {"status": charas}, follow=True)
            self.assertEqual(response.status_code,200)
            self.assertEqual(Status.objects.all().count(), 0)
            htmlresponse = response.content.decode('utf8')
            self.assertNotIn(htmlresponse, charas)
