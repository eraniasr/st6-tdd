from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
import unittest, time

class FuncTestLab7(unittest.TestCase):
    def setUp(self):
        self.browser = webdriver.Chrome(ChromeDriverManager().install())

    def tearDown(self):
        self.browser.quit()

    def test_check_title(self):
        browser = self.browser
        browser.get('http://localhost:8000')
        assert 'How are you?' in browser.title

    def test_check_background_color(self):
        browser = self.browser
        browser.get('http://localhost:8000')
        time.sleep(1)
        backgr = browser.find_element_by_class_name('w3-container')
        check_back = backgr.value_of_css_property('background-color')
        self.assertEqual('rgba(0, 0, 0, 0)', check_back)

    def test_can_submit_status(self):
        browser = self.browser
        browser.get('http://localhost:8000')
        time.sleep(1)
        status_box = browser.find_element_by_name('status')
        status_box.send_keys('no smell')
        time.sleep(3)
        status_box.submit()
        time.sleep(5)
        assert 'no smell' in browser.page_source

if __name__=='__main__':
    unittest.main(warnings='ignore')