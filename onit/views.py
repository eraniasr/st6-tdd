from django.shortcuts import render
from .forms import FormKabar
from .models import Status
from django.http import HttpResponseRedirect

# Create your views here.
def index(request):
	response = {}
	response["form"] = FormKabar
	response["status_list"] = Status.objects.all()
	if request.method == "POST":
		form_result = FormKabar(request.POST)
		if form_result.is_valid():
			Status.objects.create(status=request.POST["status"])
		return HttpResponseRedirect("/")
	return render(request, "Hai.html", response)
